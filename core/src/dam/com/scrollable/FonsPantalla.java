package dam.com.scrollable;

import com.badlogic.gdx.graphics.g2d.Batch;

import dam.com.helpers.AssetManager;

public class FonsPantalla extends Scrollable {
    public FonsPantalla(float x, float y, float amplada, float alsada, float velocitat) {
        super(x, y, amplada, alsada, velocitat);
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.disableBlending();
        batch.draw(AssetManager.fonsPantalla, posicio.x, posicio.y, amplada, alsada);
        batch.enableBlending();
    }
}
