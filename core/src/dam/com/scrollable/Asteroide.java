package dam.com.scrollable;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;

import java.util.Random;

import dam.com.helpers.AssetManager;
import dam.com.objects.Nau;
import dam.com.utils.Methods;
import dam.com.utils.Settings;

public class Asteroide extends Scrollable {

    private Circle cercle;

    public Asteroide(float x, float y, float amplada, float alsada, float velocitat) {
        super(x, y, amplada, alsada, velocitat);
        this.cercle = new Circle();
    }

    public void act(float delta){
        super.act(delta);
        this.cercle.set(posicio.x+amplada/2.0f, posicio.y + amplada/2.0f, amplada/2.0f);
    }

    public void reset(float newX){
        super.reset(newX);
        // Obtenim un número aleatori entre MIN i MAX
        float newSize = Methods.randomFloat(Settings.MIN_ASTEROIDE, Settings.MAX_ASTEROIDE);
        // Modificarem l’alçada i l’amplada segons l’aleatori anterior
        this.amplada = alsada = 34 * newSize;
        // La posició serà un valor aleatori entre 0 i l’alçada de l’aplicació menys l’alçada de l’asteroide
        this.posicio.y = new Random().nextInt(Settings.ALSADA_MAX - (int)this.alsada);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(AssetManager.asteroide, posicio.x, posicio.y, amplada, alsada);
    }

    public Circle getCercle(){
        return this.cercle;
    }

    // Retorna true si hi ha col · lisió
    public boolean xoc(Nau nau) {
        if (posicio.x <= nau.getX() + nau.getAmplada()) {
            // Comprovem si han col·lisionat sempre que l’asteroide es trobi a la mateixa alçada que l’spacecraft
            return (Intersector.overlaps(getCercle(), nau.getRectangle()));
        }
        return false;
    }


}