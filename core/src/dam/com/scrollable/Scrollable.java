package dam.com.scrollable;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Scrollable extends Actor {

    protected Vector2 posicio;
    protected float velocitat;
    protected float amplada;
    protected float alsada;
    protected boolean foraDePantalla;

    public Scrollable (float x, float y, float amplada, float alsada, float velocitat){
        this.posicio = new Vector2(x,y);
        this.velocitat = velocitat;
        this.amplada = amplada;
        this.alsada = alsada;
        this.foraDePantalla = false;
    }

    public void act(float delta){
        // Desplacem l’objecte en l’eix d’X
        this.posicio.x += this.velocitat * delta;

        // Si es troba fora de la pantalla canviem la variable a true
        if (this.posicio.x + this.amplada < 0) {
            this.foraDePantalla = true;
        }
    }

    public void reset(float novaX){
        this.posicio.x = novaX;
        this.foraDePantalla = false;
    }

    public boolean isForaDePantalla(){
        return this.foraDePantalla;
    }

    public float getTailX(){
        return this.posicio.x + this.amplada;
    }

    public float getX(){
        return this.posicio.x;
    }

    public float getY() {
        return this.posicio.y;
    }

    public float getWidth() {
        return this.amplada;
    }

    public float getHeight() {
        return this.alsada;
    }



}
