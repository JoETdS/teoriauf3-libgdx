package dam.com.helpers;

import com.badlogic.gdx.InputProcessor;

import dam.com.objects.Nau;
import dam.com.screens.GameScreen;

public class InputHandler implements InputProcessor {

    // Objectes necessaris
    private Nau nau;
    private GameScreen screen;

    // Enter per a la gestió del moviment d’arrossegament
    int Yanterior = 0;

    public InputHandler(GameScreen screen){
        this.screen = screen;
        this.nau = screen.getNau();

    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        this.Yanterior = screenY;
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // Quan deixem anar el dit acabem un moviment  i posem la nau a l’estat normal
        this.nau.endavant();
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // Posem un llindar per evitar gestionar events quan el dit està quiet
        if (Math.abs(this.Yanterior-screenY) > 2)

            // Si la Y és major que la que tenim guardada és que va cap avall
        if (this.Yanterior < screenY) {
            this.nau.abaix();
        } else {
            // En cas contrari cap amunt
            this.nau.amunt();
        }
        this.Yanterior = screenY;
        return true;
    }


    //exclusiva de les aplicacions d’escriptori, s’executa quan es
    // mou el ratolí sobre la superfície de l’aplicació.
    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }
    //exclusiva de les aplicacions d’escriptori, respon a un desplaçament de la roda del ratolí.
    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
