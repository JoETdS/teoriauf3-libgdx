package dam.com.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetManager {

    // Sprite Sheet
    public static Texture sheet;
    // Nau i fons
    public static TextureRegion nau, nauAvall, nauAmunt, fonsPantalla;
    // Asteroide
    //public static TextureRegion[] asteroide;
    public static TextureRegion asteroide;
    /*public static Animation asteroideAnim;
        // Explosió
        public static TextureRegion[] explosio;
        public static Animation explosioAnim;
    */

    public static void load(){
        // Carreguem les textures i li apliquem el mètode d’escalat ’nearest’
        sheet = new Texture(Gdx.files.internal("sheet.png"));
        sheet.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);

        // Sprites de la nau
        nau = new TextureRegion(sheet, 0, 0, 36, 15);
        nau.flip(false, true);

        nauAmunt = new TextureRegion(sheet, 36, 0, 36, 15);
        nauAmunt.flip(false, true);

        nauAvall = new TextureRegion(sheet, 72, 0, 36, 15);
        nauAvall.flip(false, true);

        // Fons de pantalla
        fonsPantalla = new TextureRegion(sheet, 0, 177, 480, 135);
        fonsPantalla.flip(false, true);

        asteroide = new TextureRegion(sheet,0,15,34,34);
        asteroide.flip(false, true);
    }

    public static void dispose(){

        // Descartem els recursos
        sheet.dispose();

    }
}
