package dam.com.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import dam.com.helpers.AssetManager;
import dam.com.utils.Settings;

public class Nau extends Actor {

    //defenim les diferents opcions que podrà fer una nau com a constants
    public static final int ENDAVANT = 0;
    public static final int PUJAR = 1;
    public static final int BAIXAR = 2;

    //paràmetres de la nau
    private Vector2 posicio;
    private int amplada, alsada; //ja sé que hi ha una falta d'ortografia
    private int direccio;
    private Rectangle rectangle;

    public Nau(float x, float y, int amplada, int alsada){
        this.amplada = amplada;
        this.alsada = alsada;
        this.posicio = new Vector2(x,y);
        this.direccio = ENDAVANT;

        // Creem el rectangle de col·lisions
        this.rectangle = new Rectangle();

    }

    public void act(float delta){
        //movem la nau sense sortir de pantalla
        switch (this.direccio){
            case PUJAR:
                if(this.posicio.y - Settings.VELOCITAT_NAU * delta >= 0){
                    this.posicio.y -= Settings.VELOCITAT_NAU * delta;
                }
                break;
            case BAIXAR:
                if(this.posicio.y + this.alsada + Settings.VELOCITAT_NAU * delta <= Settings.ALSADA_MAX){
                    this.posicio.y += Settings.VELOCITAT_NAU * delta;
                }
                break;
            case ENDAVANT:
                break;
        }
        this.rectangle.set(posicio.x, posicio.y + 3, amplada, alsada);
    }

    public Rectangle getRectangle(){
        return this.rectangle;
    }

    public void amunt(){
        this.direccio = PUJAR;
    }

    public void abaix(){
        this.direccio = BAIXAR;
    }

    public void endavant(){
        this.direccio = ENDAVANT;
    }

    public float getX() {
        return this.posicio.x;
    }

    public float getY() {
        return this.posicio.y;
    }


    public int getAmplada() {
        return this.amplada;
    }


    public int getAlsada() {
        return this.alsada;
    }

    public void draw(Batch batch, float parentAlpha){
        super.draw(batch, parentAlpha);
        batch.draw(AssetManager.nau, posicio.x, posicio.y, amplada, alsada);
    }

    // Obtenim el TextureRegion depenent de la posició de la spacecraft
    public TextureRegion getSpacecraftTexture() {

        switch (direccio) {

            case ENDAVANT:
                return AssetManager.nau;
            case PUJAR:
                return AssetManager.nauAmunt;
            case BAIXAR:
                return AssetManager.nauAvall;
            default:
                return AssetManager.nau;
        }
    }

}
