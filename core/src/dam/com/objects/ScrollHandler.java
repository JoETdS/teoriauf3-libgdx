package dam.com.objects;

import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;
import java.util.Random;

import dam.com.scrollable.Asteroide;
import dam.com.scrollable.FonsPantalla;
import dam.com.utils.Methods;
import dam.com.utils.Settings;


//Group és una classe que aglutina diferents actors
public class ScrollHandler extends Group {

    //Fons de pantalla
    FonsPantalla fonsPantalla, fonsPantallaNegre;

    //Asteroides
    int numAsteroides;
    ArrayList<Asteroide> asteroides;

    //Objecte random
    Random rand;

    public ScrollHandler() {
        this.fonsPantalla = new FonsPantalla(0, 0, Settings.AMPLADA_MAX * 2,
                Settings.ALSADA_MAX, Settings.VELOCITAT_FONSPANTALLA);
        this.fonsPantallaNegre = new FonsPantalla(this.fonsPantalla.getTailX(), 0,
                Settings.AMPLADA_MAX * 2, Settings.ALSADA_MAX, Settings.VELOCITAT_FONSPANTALLA);

        //afegim actors al grup
        addActor(this.fonsPantalla);
        addActor(this.fonsPantallaNegre);

        // Creem l’objecte random
        this.rand = new Random();

        // Comencem amb 3 asteroides
        this.numAsteroides = 3;

        // Creem l’ArrayList
        this.asteroides = new ArrayList<Asteroide>();

        // Definim una mida aleatòria entre el mínim i el màxim
        float newSize = Methods.randomFloat(Settings.MIN_ASTEROIDE, Settings.MAX_ASTEROIDE) * 34;

        // Afegim el primer asteroide a l’array i al grup
        Asteroide asteroide = new Asteroide(Settings.AMPLADA_MAX,
                this.rand.nextInt(Settings.AMPLADA_MAX - (int) newSize),
                newSize, newSize, Settings.VELOCITAT_ASTEROIDE);
        this.asteroides.add(asteroide);
        addActor(asteroide);

        // Des del segon fins l’últim asteroide
        for (int i = 1; i < this.numAsteroides; i++) {
            // Creem la mida aleatòria
            newSize = Methods.randomFloat(Settings.MIN_ASTEROIDE, Settings.MAX_ASTEROIDE) * 34;
            // Afegim l’asteroide
            asteroide = new Asteroide(asteroides.get(asteroides.size() - 1).getTailX() + Settings.ASTEROID_GAP,
                    this.rand.nextInt(Settings.ALSADA_MAX - (int) newSize), newSize, newSize,
                    Settings.VELOCITAT_ASTEROIDE);
            // Afegim l’asteroide a l’ArrayList
            asteroides.add(asteroide);
            // Afegim l’asteroide al grup d’actors
            addActor(asteroide);
        }
    }


    @Override
    public void act(float delta) {
        super.act(delta);
        // Si algun element es troba fora de la pantalla, fem un reset de l’element
        if (this.fonsPantalla.isForaDePantalla()) {
            this.fonsPantalla.reset(this.fonsPantallaNegre.getTailX());
        } else if (this.fonsPantallaNegre.isForaDePantalla()) {
            this.fonsPantallaNegre.reset(this.fonsPantalla.getTailX());
        }
        for (int i = 0; i < this.asteroides.size(); i++) {
            Asteroide asteroid = this.asteroides.get(i);
            if (asteroid.isForaDePantalla()) {
                if (i == 0) {
                    asteroid.reset(this.asteroides.get(this.asteroides.size() - 1).getTailX() + Settings.ASTEROID_GAP);
                } else {
                    asteroid.reset(this.asteroides.get(i - 1).getTailX() + Settings.ASTEROID_GAP);
                }
            }
        }
    }
    public ArrayList<Asteroide> getAsteroids() {
        return this.asteroides;
    }

    public boolean xoc(Nau nau) {
        // Comprovem les col·lisions entre cada asteroide i la nau
        for (Asteroide asteroid : asteroides) {
            if (asteroid.xoc(nau)) {
                return true;
            }
        }
        return false;
    }


}
