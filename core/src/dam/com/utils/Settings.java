package dam.com.utils;

public class Settings {

    //Mesures pantalla

    public static final int AMPLADA_MAX = 240;
    public static final int ALSADA_MAX = 135;

    //Mesures nau
    public static final float VELOCITAT_NAU = 50;
    public static final int AMPLADA_NAU = 36;
    public static final int ALSADA_NAU = 15;
    public static final float NAU_X_INICIAL = 20;
    public static final float NAU_Y_INICIAL = ALSADA_MAX/2 - ALSADA_NAU/2;

    //Mesures asteroide
    public static final float MAX_ASTEROIDE = 1.5f;
    public static final float MIN_ASTEROIDE = 0.5f;
    public static final int VELOCITAT_ASTEROIDE = -150;
    public static final int ASTEROID_GAP = 75;
    public static final int VELOCITAT_FONSPANTALLA = -100;

}
