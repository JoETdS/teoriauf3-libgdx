package dam.com;

import com.badlogic.gdx.Game;

import dam.com.helpers.AssetManager;
import dam.com.screens.GameScreen;

public class JocProva extends Game {
	
	@Override
	public void create () {
		//a l'iniciar el joc carreguem els recursos
		AssetManager.load();
		//definim la pantalla principal
		setScreen(new GameScreen());
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		super.dispose();
		AssetManager.dispose();
	}
}
