package dam.com.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.util.ArrayList;

import dam.com.helpers.InputHandler;
import dam.com.objects.Nau;
import dam.com.objects.ScrollHandler;
import dam.com.scrollable.Asteroide;
import dam.com.utils.Settings;


//https://libgdx.badlogicgames.com/ci/nightlies/docs/api/com/badlogic/gdx/Screen.html
//explicació dels mètodes de la classe Screen
public class GameScreen implements Screen {

    private Stage stage;
    private Nau nau;
    private ScrollHandler scrollHandler;
    private boolean gameOver;


    // Representació de figures geomètriques
    private ShapeRenderer shapeRenderer;
    // Per obtenir el batch de l’stage
    private Batch batch;

    public GameScreen(){

        //Creem el shapeRender
        this.shapeRenderer = new ShapeRenderer();

        //Marquem que el joc no ha acabat
        this.gameOver = false;

        //OrthographicCamera és una eina per determinar allò que pot veure el jugador (només en 2D)
        // Creem la càmera de les dimensions del joc
        OrthographicCamera camera = new OrthographicCamera(Settings.AMPLADA_MAX,Settings.ALSADA_MAX);
        // Posant el paràmetre a true configurem la càmera perquè faci servir el sistema de coordenades Y-Down
        camera.setToOrtho(true);

        // Creem el viewport amb les mateixes dimensions que la càmera
        StretchViewport viewport = new StretchViewport(Settings.AMPLADA_MAX,Settings.ALSADA_MAX, camera);

        //creem un stage, que és qui gestiona els actors(qui pot realitzar accions) i esdeveniments
        //Afegirem el viewport al Stage
        this.stage = new Stage(viewport);

        this.batch = this.stage.getBatch();

        //iniciem els actors de la pantalla
        this.nau = new Nau(Settings.NAU_X_INICIAL,
                            Settings.NAU_Y_INICIAL,
                            Settings.AMPLADA_NAU,
                            Settings.ALSADA_NAU);

        this.scrollHandler = new ScrollHandler();

        //afegim els actors
        this.stage.addActor(this.scrollHandler);
        this.stage.addActor(this.nau);
        this.nau.setName("nau");

        // Assignem com a gestor d’entrada la classe InputHandler
        Gdx.input.setInputProcessor(new InputHandler(this));
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        // Dibuixem i actualitzem tots els actors de l’stage
        this.stage.draw();
        this.stage.act(delta);

        if(!gameOver){
            //si no s'ha acabat el joc, comprovem si xoquem
            if (scrollHandler.xoc(nau)) {
                this.gameOver = true;
                this.stage.getRoot().findActor("nau").remove();
            }
        }else{
            BitmapFont font = new BitmapFont(true);
            batch.begin();
            font.draw(batch, "GameOver", 10, 10);
            batch.end();
        }

        //dibuixem els elements
        //dibuixarElements();
    }

    private void dibuixarElements() {

        /* 1 */
        // Pintem el fons de negre per evitar el "flickering"
        Gdx.gl20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        /* 2 */
        // Recollim les propietats del Batch de l’Stage
        this.shapeRenderer.setProjectionMatrix(this.batch.getProjectionMatrix());
        // Inicialitzem el shaperenderer
        this.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        /* 3 */
        // Definim el color (verd)
        this.shapeRenderer.setColor(new Color(0, 1, 0, 1));
        // Pintem la nau
        this.shapeRenderer.rect(this.nau.getX(), this.nau.getY(), this.nau.getAmplada(), this.nau.getAlsada());

        /* 4 */
        // Recollim tots els Asteroid
        ArrayList<Asteroide> asteroides = this.scrollHandler.getAsteroids();
        Asteroide asteroide;

        //pintem els asteroides

        for (int i = 0; i < asteroides.size(); i++) {
            asteroide = asteroides.get(i);
            switch (i) {
                case 0:
                    //pintem el primer de color vermell
                    this.shapeRenderer.setColor(1,0,0,1);
                    break;
                case 1:
                    //pintem el segon de color blau
                    this.shapeRenderer.setColor(0,0,1,1);
                    break;
                case 2:
                    //pintem el tercer de color groc
                    this.shapeRenderer.setColor(1,1,0,1);
                    break;
                default:
                    //pintem la resta de color blanc
                    this.shapeRenderer.setColor(1,1,1,1);
                    break;
            }
            this.shapeRenderer.circle(asteroide.getX() + asteroide.getWidth()/2,
                    asteroide.getY() + asteroide.getWidth()/2, asteroide.getWidth()/2);
        }
        /* 5 */
        this.shapeRenderer.end();

    }

    public Nau getNau(){
        return this.nau;
    }

    public Stage getStage(){
        return this.stage;
    }

    public ScrollHandler getScrollHandler(){
        return this.scrollHandler;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
